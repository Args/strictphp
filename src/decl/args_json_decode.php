<?hh //strict

/**
 * @package StrictPHP
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 09/Jan/2019
 */

namespace Args\StrictPHP;

use type Args\Failable\May;
use type Args\Failable\UnknownException;

/**
 * Improved json_decode to return a map instead of who knows what.
 * If the json was a scalar or null, May::empty will be returned, unless
 * your fourth argument $headup is callable. In that case, we'll call
 * the callable if the json decodes to a non-collection and return a map
 * like Map{scalar => $value}. Just like a PHP object cast would.
 * The values in the map are still their original types
 */
function args_json_decode(
    string $json,
    int $depth = 512,
    int $options = 0,
    ?(function(): void) $headsup = null,
): May<Map<arraykey, mixed>> {
    $result = json_decode($json, true, $depth, $options);

    if (!$result->isset()) {
        return May::empty($result->getExceptionReq());
    }

    $result = $result->getValue();

    if ($result === null || \is_scalar($result)) {
        if ($headsup === null) {
            return May::empty(new \InvalidArgumentException(
                "Scalar decoded from json. If you wish to have a result anyway, read the docblock for args_json_decode.",
            ));
        }
        $headsup();
        return May::full(Map {'scalar' => $result});
    }

    if (!\is_array($result)) {
        \trigger_error(
            "Expected array, but something went wrong. Please make bug report.",
            \E_USER_WARNING,
        );
        return May::empty(new UnknownException());
    }

    return May::full(new Map($result));
}
