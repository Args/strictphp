<?hh //strict

/**
 * @package StrictPHP
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 09/Jan/2019
 */

namespace Args\StrictPHP;

use type Args\Failable\May;
use type Args\Failable\UnknownException;

/**
 * @see https://secure.php.net/manual/en/function.file-get-contents.php
 */
function file_get_contents(
    string $filename,
    ?bool $use_include_path = null,
    ?resource $context = null,
    ?int $offset = null,
    ?int $maxlen = null,
): May<string> {
    if ($use_include_path === null) {
        $value = \file_get_contents($filename);
    } elseif ($context === null) {
        $value = \file_get_contents($filename, $use_include_path);
    } elseif ($offset === null) {
        $value = \file_get_contents($filename, $use_include_path, $context);
    } elseif ($maxlen === null) {
        $value =
            \file_get_contents($filename, $use_include_path, $context, $offset);
    } else {
        $value = \file_get_contents(
            $filename,
            $use_include_path,
            $context,
            $offset,
            $maxlen,
        );
    }

    if ($value === false) {
        return May::empty(
            __error_get_last_to_errorexception() ?? new UnknownException(),
        );
    }

    //Fail loud if the the documentation was wrong and string|false is not the return type of file_get_contents.
    return May::full($value as string);
}
