<?hh //strict

/**
 * @package StrictPHP
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 09/Jan/2019
 */

namespace Args\StrictPHP;

use type Args\Failable\May;
use type Args\Failable\UnknownException;

/**
 * @see https://secure.php.net/manual/en/function.hash-hmac.php
 */
function hash_hmac(
    string $algo,
    string $data,
    string $key,
    bool $raw_output = false,
): May<string> {
    $result = \hash_hmac($algo, $data, $key, $raw_output);

    if (!($result is string)) {
        return May::empty(
            __error_get_last_to_errorexception() ?? new UnknownException(),
        );
    }

    //Fail load if the documentation was incorrect.
    return May::full($result as string);
}
