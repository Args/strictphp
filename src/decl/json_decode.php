<?hh //strict

/**
 * @package StrictPHP
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 09/Jan/2019
 */

namespace Args\StrictPHP;

use type Args\Failable\May;

/**
 * Decodes json without having to check for "null" first or \json_last_error() afterwards.
 * @see https://secure.php.net/manual/en/function.json-decode.php
 * DO NOT use this from PHP 7.3.
 * We presume that the constant \JSON_THROW_ON_ERROR does not exist and will not throw.
 * @return May<array|scalar|\stdClass|null>
 */
function json_decode(
    string $json,
    bool $assoc = false,
    int $depth = 512,
    int $options = 0,
): May<mixed> {
    $result = \json_decode($json, $assoc, $depth, $options);
    if ($result !== null) {
        return May::full($result);
    }
    if ($result === null && $json === 'null') {
        return May::full($result);
    }

    $err = \json_last_error();
    $textual = \json_last_error_msg();

    //Fail loud if the documentation of json_last_error() was misleading.
    $message = $err as int;
    $textual = $textual as string;
    return May::empty(
        new \InvalidArgumentException(
            \sprintf("json_decode failed: %s", $textual),
            $message,
        ),
    );
}
