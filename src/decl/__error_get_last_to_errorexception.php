<?hh //strict

/**
 * @package StrictPHP
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 09/Jan/2019
 */

namespace Args\StrictPHP;

/**
 * Does nothing.
 */
function __error_get_last_to_errorexception(): ?\ErrorException {
    $err = \error_get_last();
    if ($err === null) {
        return null;
    }

    //Fail loud if the documentation of error_get_last was wrong.
    $message = $err['message'] as string;
    $code = $err['code'] as int;
    $severity = \E_USER_WARNING;
    $file = $err['file'] as string;
    $line = $err['line'] as int;
    return new \ErrorException($message, $code, $severity, $file, $line);
}
